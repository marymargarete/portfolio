// JavaScript Document
$( document ).ready(function() {
	if($(".splash").is(":visible"))
	{
		$(".wrapper").css({"opacity":"0"});
	}
	$(".splash-arrow").click(function()
	{
		$(".splash").slideUp("800", function() {
			$(".wrapper").delay(100).animate({"opacity":"1.0"},800);
		});
	});
});

$(window).scroll(function() {
	$(window).off("scroll");
	$(".splash").slideUp("800", function() {
	$("html, body").animate({"scrollTop":"0px"},100);
	$(".wrapper").delay(90).animate({"opacity":"1.0"},800);
 });
 });

$(document).on("scroll", function(){
	var scrollTop = $(document).scrollTop()
	var scrollBottom = scrollTop + $(window).height()
	
	var pageBottom = $(document).height()
	
	var diff = pageBottom - scrollBottom
	
	var opacity = 1 - diff/290
	
	$("div.fadebg").css("opacity", opacity)
})

$("#menu").click(function () {
            $(".main-nav").toggleClass('is-open');
            $(".overlay").toggleClass('is-visible');
			$(".about").toggleClass('is-invisible');
			$("#main-content").toggleClass('is-invisible');
            $(this).toggleClass('open');
 });

$("#resourcesmenu").click(function () {
	$(".main-nav").toggleClass('is-open');
	$(".resources").toggleClass('is-invisible');
            $(this).toggleClass('open');
	});


